# || About The Game || #

Multiplayer Racing alpha was created by James Gilsenan and Kenneth Clifford.
It is an multiplayer networking game where players compete against each other.
They must race around the track. The person who completes a lap of the track wins.
Make sure you use your missiles to slow your opponents down.
The game utilizes reliable UDP to send packets between the clients and server. 

# || Controls || #

Use W to move up
Use A to move up
Use S to move up
Use D to move up
Use Space to shoot

# || Instructions || #

To run the game:
1. copy the sfml folder to your c: drive
2. Copy the .dll files from the sfml bin folder into the projects Debug folder
3. Open the visual studio project in 2015
4. Build the code
5. Right click the server project and go to debug, run new instance
6. Right click the client project and go to debug, run new instance

Enjoy


Future improvements for the game could include:

1. Pickups that were taken respawn after 3 seconds
2. StartRace method so the players cannot move until the race countdown is initiated by all players being ready
3. Leaderboard that saves to file the race winner and if they are already present, update their number of wins