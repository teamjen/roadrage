//Chnages by James
class MouseServer : public Mouse
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new MouseServer() ); }
	void HandleDying() override;
	virtual bool		HandleCollisionWithCat( RoboCat* inCat ) override;

protected:
	MouseServer();

//variables for setting the top speed allowed and the current speed of player
private:
	BYTE mTopSpeed;
	BYTE mSpeedIncrease;
};