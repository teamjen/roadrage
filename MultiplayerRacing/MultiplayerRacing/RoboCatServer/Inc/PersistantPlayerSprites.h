// Written by: Ronan
//Changes by James

class PersistantPlayerSprites : public RoboCat
{
public:
	static void StaticInit();
	static std::unique_ptr<PersistantPlayerSprites> sInstance;

	void Load();
	void AddPlayerEntry(std::string p_name, int p_id);
	int GetID(std::string p_name);

	void LoadLeaderBoardFile();
	//void AddWinnerEntry();

private:
	std::unordered_map<string, int> m_idMaps;
};