//Changes by James
class Server : public Engine
{
public:

	static bool StaticInit();

	virtual void DoFrame() override;

	virtual int Run();

	void HandleNewClient( ClientProxyPtr inClientProxy );
	void HandleLostClient( ClientProxyPtr inClientProxy );

	RoboCatPtr	GetCatForPlayer( int inPlayerId );
	void	SpawnCatForPlayer( int inPlayerId );
	//Method for spawning pickups
	void	SpawnPickup( int pickupCount);

private:
	Server();
	float   TimeBetweenSpawns;
	float	SpawnTime;
	//index used for iterating through m_spawnPoints
	int		spawnIndex;
	bool	InitNetworkManager();
	void	PickupUpdate();
	void	SetupWorld();



	std::vector<Vector3> m_spawnPoints;
	std::vector<Vector3> m_pickupPoints;
	

};