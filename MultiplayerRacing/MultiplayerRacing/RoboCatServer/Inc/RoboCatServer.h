//Changes by James
enum ECatControlType
{
	ESCT_Human,
	ESCT_AI
};

class RoboCatServer : public RoboCat
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new RoboCatServer() ); }
	virtual void HandleDying() override;

	virtual void Update();

	void SetCatControlType( ECatControlType inCatControlType ) { mCatControlType = inCatControlType; }

	void TakeDamage( int inDamagingPlayerId );
	//Method for broadcasting to all clients that game is over
	void SetIsOver();
	void SetWeWon(bool won);


protected:
	RoboCatServer();

private:

	void HandleShooting();

	ECatControlType	mCatControlType;


	float		mTimeOfNextShot;
	float		mTimeBetweenShots;
	//James - varibales for setting lowest posssible speed of player and how much speed they lose per hit
	BYTE		mMaxSpeedDecrease;
	BYTE		mLowestSpeed;

};