//Changes by James
#include <RoboCatServerPCH.h>

RoboCatServer::RoboCatServer() :
	mCatControlType( ESCT_Human ),
	mTimeOfNextShot( 0.f ),
	mTimeBetweenShots( 1.5f ),
	mMaxSpeedDecrease( 1 ),
	mLowestSpeed( 6 )
{}

//James - method to update all clients that you have won the race and the game is over
void RoboCatServer::SetIsOver()
{
	//if you have finished the race and set the wi logic locally, tell everyone the race is over
	//only the player who broadcasts this wins because other clients will not have their weWon bool set to true
	if (GetIsGameOver())
	{
		SetIsGameOver(true);
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_GameOver);
	}
}

void RoboCatServer::SetWeWon(bool won)
{
	//if you have finished the race and set the wi logic locally, tell everyone the race is over
	//only the player who broadcasts this wins because other clients will not have their weWon bool set to true
	if (GetWeAreTheWinner())
	{
		SetIsGameOver(true);
		SetWeWon(true);
		NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_GameOver);
	}
}

void RoboCatServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void RoboCatServer::Update()
{
	RoboCat::Update();
	
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();
	float oldRotation = GetRotation();

	//are you controlled by a player?
	//if so, is there a move we haven't processed yet?
	if( mCatControlType == ESCT_Human )
	{
		ClientProxyPtr client = NetworkManagerServer::sInstance->GetClientProxy( GetPlayerId() );
		if( client )
		{
			MoveList& moveList = client->GetUnprocessedMoveList();
			for( const Move& unprocessedMove : moveList )
			{
				const InputState& currentState = unprocessedMove.GetInputState();

				float deltaTime = unprocessedMove.GetDeltaTime();

				ProcessInput( deltaTime, currentState );
				SimulateMovement( deltaTime );

				//LOG( "Server Move Time: %3.4f deltaTime: %3.4f left rot at %3.4f", unprocessedMove.GetTimestamp(), deltaTime, GetRotation() );

			}

			moveList.Clear();
		}
	}
	else
	{
		//do some AI stuff
		SimulateMovement( Timing::sInstance.GetDeltaTime() );
	}

	HandleShooting();

	if( !RoboMath::Is2DVectorEqual( oldLocation, GetLocation() ) ||
		!RoboMath::Is2DVectorEqual( oldVelocity, GetVelocity() ) ||
		oldRotation != GetRotation() )
	{
		NetworkManagerServer::sInstance->SetStateDirty( GetNetworkId(), ECRS_Pose );
	}

}

void RoboCatServer::HandleShooting()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if( mIsShooting && Timing::sInstance.GetFrameStartTime() > mTimeOfNextShot )
	{
		//not exact, but okay
		mTimeOfNextShot = time + mTimeBetweenShots;

		//fire!
		YarnPtr yarn = std::static_pointer_cast< Yarn >( GameObjectRegistry::sInstance->CreateGameObject( 'YARN' ) );
		yarn->InitFromShooter( this );
	}
}

void RoboCatServer::TakeDamage( int inDamagingPlayerId )
{
	if(GetMaxSpeed() > mLowestSpeed)
	GetMaxSpeed() -= mMaxSpeedDecrease;

	NetworkManagerServer::sInstance->SetStateDirty(GetNetworkId(), ECRS_Speed);
}
