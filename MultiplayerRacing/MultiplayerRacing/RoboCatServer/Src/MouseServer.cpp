//Changes by James
#include <RoboCatServerPCH.h>

//Speed variables added
MouseServer::MouseServer() :
	mTopSpeed(12),
	mSpeedIncrease(2)
{}

void MouseServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}


bool MouseServer::HandleCollisionWithCat( RoboCat* inCat )
{
	if (!picked)
	{
		//James - Increase the players speed here
		//Max possible speed for the player set here
		//if they are below topSpeed, increase their speed
		if (inCat->GetMaxSpeed() <= mTopSpeed)
			inCat->GetMaxSpeed() += mSpeedIncrease;
		picked = true;
		
		// Hacked in here.
		int ECRS_Speed = 1 << 3;
		NetworkManagerServer::sInstance->SetStateDirty(inCat->GetNetworkId(), ECRS_Speed);
	}
	//kill yourself!
	SetDoesWantToDie( true );
	return false;
}


