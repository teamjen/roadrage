//Changes by James
#include <RoboCatServerPCH.h>


bool Server::StaticInit()
{
	ShadowFactory::StaticInit();
	ConnectionDetails::StaticInit();
	PersistantPlayerSprites::StaticInit();
	//LeaderBoard::StaticInit();

	sInstance.reset( new Server() );
	return true;
}

Server::Server()
{
	int spawnIndex = 0;

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnServer::StaticCreate );

	InitNetworkManager();

	TimeBetweenSpawns = 1.f;
	SpawnTime = 0.f;
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );

	//James - Created spawn points for pickups
	m_pickupPoints = {
		Vector3(500, 1065, 0),
		Vector3(500, 985, 0),
		Vector3(100, 300, 0),
		Vector3(210, 300, 0),
		Vector3(1100, 680, 0),
		Vector3(1100, 600, 0),
		Vector3(1625, 400, 0),
		Vector3(1695, 400, 0),
		Vector3(1400, 1065, 0),
		Vector3(1400, 985, 0),

	};
	//James - created spawn points for players
	m_spawnPoints = {
		Vector3(1060, 1070, 0),
		Vector3(1060, 1025, 0),
		Vector3(1060, 980, 0),

	};

}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	uint16_t port = ConnectionDetails::sInstance->GetServerPort();
	return NetworkManagerServer::StaticInit( port );
}


void Server::PickupUpdate()
{
	float time = Timing::sInstance.GetFrameStartTime();
	if (Timing::sInstance.GetFrameStartTime() > SpawnTime)
	{
		SpawnTime = time + TimeBetweenSpawns;
		SpawnPickup(1);
	}
}

void Server::SetupWorld()
{
	//spawn some pickups
	SpawnPickup(1);
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
	
	this->PickupUpdate();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{
	
	int playerId = inClientProxy->GetPlayerId();
	
	ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnCatForPlayer( playerId );

}

void Server::SpawnCatForPlayer( int inPlayerId )
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >( GameObjectRegistry::sInstance->CreateGameObject( 'RCAT' ) );
	cat->SetColor( ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	cat->SetPlayerId( inPlayerId );
	
	// Pick one of a few random locations.
	int randomIndex = rand() % m_spawnPoints.size();
	cat->SetLocation( m_spawnPoints[randomIndex] );

}

//James - spawn pickups in their locations only once
void Server::SpawnPickup(int pickupCount)
{
	GameObjectPtr pickup;
	pickup = GameObjectRegistry::sInstance->CreateGameObject('MOUS');
	float size = 32;
	
	//if the all the pickups has been spawned once, dont spawn anymore
	if (spawnIndex < m_pickupPoints.size())
	{
		//Iterate through the array of spawn points
		Vector3 pickupLocation = m_pickupPoints[spawnIndex];
		(ShadowFactory::sInstance->doesCollideWithWorld(sf::FloatRect(pickupLocation.mX - (size / 2), pickupLocation.mY - (size / 2), size, size)));
		//span the pickup at the location
		pickup->SetLocation(pickupLocation);
		//increment the index to spawn pickup at the next location
		spawnIndex++;
	}
	else
	{
		//don't spawn anything
	}
	
}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry( playerId );
	RoboCatPtr cat = GetCatForPlayer( playerId );
	if( cat )
	{
		cat->SetDoesWantToDie( true );
	}
}

RoboCatPtr Server::GetCatForPlayer( int inPlayerId )
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];
		RoboCat* cat = go->GetAsCat();
		if( cat && cat->GetPlayerId() == inPlayerId )
		{
			return std::static_pointer_cast< RoboCat >( go );
		}
	}

	return nullptr;

}