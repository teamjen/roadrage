// Changes made by James
#include <RoboCatClientPCH.h>
#include <iostream>

std::unique_ptr< SFRenderManager >	SFRenderManager::sInstance;

SFRenderManager::SFRenderManager()
{
	// Might need some view stuff in here or something.
	view.reset(sf::FloatRect(0, 0, 800, 600));
	SFWindowManager::sInstance->setView(view);
	m_startScreen.setTexture(*SFTextureManager::sInstance->GetTexture("connectingToServer"));
	m_loseScreen.setTexture(*SFTextureManager::sInstance->GetTexture("lose"));
	m_winnerScreen.setTexture(*SFTextureManager::sInstance->GetTexture("win"));
	checkpoint1 = false;
	checkpoint2 = false;
	checkpoint3 = false;
	finishline1 = false;
	finishline2 = false;
	raceFinished = false;
	weWon = false;
	BYTE currentLap;
	BYTE totalLaps;
}


//James - method to check if the race has been finished and if we finished first
void SFRenderManager::GameIsOver()
{
	RaceProgress();

	if (raceFinished == true)
	{
		this->SetWeAreTheWinner(true);
	}
	//James - Taken from find cat center ,ethod
	//Iterate through the game objects in the world
	uint32_t catID = (uint32_t)'RCAT';
	for (auto obj : World::sInstance->GetGameObjects())
	{
		//James - Find player objects
		if (obj->GetClassId() == catID)
		{
			//Find our id by checking the player ids and checking if they match our netork id
			RoboCat *cat = dynamic_cast<RoboCat*>(obj.get());
			auto id = cat->GetPlayerId();
			auto ourID = NetworkManagerClient::sInstance->GetPlayerId();
			//We are the player who finished first, therfore we won
			//set the game over to true so everybody knows the game is over
			if (id == ourID && raceFinished == true)
			{
				weWon = true;
				this->SetWeAreTheWinner(true);
				this->SetIsGameOver(true);
				
			}
			else if (GetIsGameOver())
			{
				weWon = false;
				//this->SetWeAreTheWinner(true);
				this->SetIsGameOver(true);
			}
		}
	}
}


//James - Added lap counter to display what lap player is on
void SFRenderManager::RenderUI()
{	
	//James - update the lap counter
	LapCounter();

	sf::Font bebas = *FontManager::sInstance->GetFont("bebas");

	//James - set text, position, font, size, color, draw for lap counter
	sf::Text RTT, InOut, Checkpoint, Laps;

	sf::Vector2f basePos(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);

	RTT.setPosition(basePos.x + 20, basePos.y + 20);
	InOut.setPosition(basePos.x + 120, basePos.y + 20);
	Laps.setPosition(basePos.x + 650, basePos.y + 10);

	RTT.setFont(bebas);
	InOut.setFont(bebas);
	Laps.setFont(bebas);

	RTT.setCharacterSize(24);
	InOut.setCharacterSize(24);
	Laps.setCharacterSize(48);

	RTT.setColor(sf::Color::Blue);
	InOut.setColor(sf::Color::Blue);
	Laps.setColor(sf::Color::Blue);

	// RTT
	float rttMS = NetworkManagerClient::sInstance->GetAvgRoundTripTime().GetValue() * 1000.f;
	string roundTripTime = StringUtils::Sprintf("RTT %d ms", (int)rttMS);
	RTT.setString(roundTripTime);

	// Bandwidth
	string bandwidth = StringUtils::Sprintf("In %d  Bps, Out %d Bps",
		static_cast<int>(NetworkManagerClient::sInstance->GetBytesReceivedPerSecond().GetValue()),
		static_cast<int>(NetworkManagerClient::sInstance->GetBytesSentPerSecond().GetValue()));
	InOut.setString(bandwidth);

	//James - Laps counter
	string lapCounter = StringUtils::Sprintf("Lap %d / %d",
		currentLap, 
		totalLaps);
	Laps.setString(lapCounter);

	
	// Draw the text to screen.
	SFWindowManager::sInstance->draw(RTT);
	SFWindowManager::sInstance->draw(InOut);
	SFWindowManager::sInstance->draw(Laps);
	
}


void SFRenderManager::UpdateView()
{
	// Lower rate means more 'lag' on the camera following the player.
	float rate = .02f;
	if (FindCatCentre() != sf::Vector2f(-1, -1))
	{
		sf::Vector2f player = FindCatCentre();
		sf::Vector2f newCentre = view.getCenter() + ((player - view.getCenter()) * rate);
		view.setCenter(newCentre);
	}
	SFWindowManager::sInstance->setView(view);
}

void SFRenderManager::RenderTexturedWorld()
{
	for (auto spr : TexturedWorld::sInstance->getTexturedWorld())
	{
		SFWindowManager::sInstance->draw(spr);
	}
}


//James - method for checking when player has crossed checkpoints and completed laps
void SFRenderManager::RaceProgress()
{

	//James - checkpoint on first lap was passed
	if (FindCatCentre().x > 690.f && FindCatCentre().x < 900.f &&
		FindCatCentre().y < 500.f && FindCatCentre().y > 460.f)
	{
		checkpoint1 = true;
	}
	//James - finishline on first lap was passed
	if (checkpoint1 == true &&
		FindCatCentre().x < 1020.f && FindCatCentre().x > 980.f &&
		FindCatCentre().y < 1090.f && FindCatCentre().y > 960.f)
	{
		finishline1 = true;
		//Debugging
		//raceFinished = true;
	}
	//James - checkpoint on second lap was passed
	if (finishline1 == true &&
		FindCatCentre().x > 690.f && FindCatCentre().x < 900.f &&
		FindCatCentre().y < 500.f && FindCatCentre().y > 460.f)
	{
		checkpoint2 = true;
	}
	//James - finishline on second lap was passed
	if (checkpoint2 == true &&
		FindCatCentre().x < 1020.f && FindCatCentre().x > 980.f &&
		FindCatCentre().y < 1090.f && FindCatCentre().y > 960.f)
	{
		finishline2 = true;
	}
	//James - checkpoint on third lap was passed
	if (finishline2 == true &&
		FindCatCentre().x > 690.f && FindCatCentre().x < 900.f &&
		FindCatCentre().y < 500.f && FindCatCentre().y > 460.f)
	{
		checkpoint3 = true;
	}
	//if the finishline has been crossed on the final lap, race is finished
	if (checkpoint3 == true &&
		FindCatCentre().x < 1020.f && FindCatCentre().x > 980.f &&
		FindCatCentre().y < 1090.f && FindCatCentre().y > 960.f)
	{
		raceFinished = true;
	}
}

//James - Method for keeping track of laps
void SFRenderManager::LapCounter()
{
	RaceProgress();

	//James - if the player finished lap 1, increment the lap counter
	if (finishline1 == true)
		currentLap = 2;
	
	//James - if the player finished lap 2, increment the lap counter
	if (finishline2 == true)
		currentLap = 3;
	
}


sf::Vector2f SFRenderManager::FindCatCentre()
{
	uint32_t catID = (uint32_t)'RCAT';
	for (auto obj : World::sInstance->GetGameObjects())
	{
		// Find a cat.
		if (obj->GetClassId() == catID)
		{
			RoboCat *cat = dynamic_cast<RoboCat*>(obj.get());
			auto id = cat->GetPlayerId();
			auto ourID = NetworkManagerClient::sInstance->GetPlayerId();
			if (id == ourID)
			{
				// If so grab the centre point.
				auto centre = cat->GetLocation();
				m_lastCatPos.x = centre.mX;
				m_lastCatPos.y = centre.mY;
				return sf::Vector2f(centre.mX, centre.mY);
			}
		}
	}
	return sf::Vector2f(-1, -1);
}


void SFRenderManager::StaticInit()
{
	sInstance.reset(new SFRenderManager());
}


void SFRenderManager::AddComponent(SFSpriteComponent* inComponent)
{
	mComponents.push_back(inComponent);
}

void SFRenderManager::RemoveComponent(SFSpriteComponent* inComponent)
{
	int index = GetComponentIndex(inComponent);

	if (index != -1)
	{
		int lastIndex = mComponents.size() - 1;
		if (index != lastIndex)
		{
			mComponents[index] = mComponents[lastIndex];
		}
		mComponents.pop_back();
	}
}

int SFRenderManager::GetComponentIndex(SFSpriteComponent* inComponent) const
{
	for (int i = 0, c = mComponents.size(); i < c; ++i)
	{
		if (mComponents[i] == inComponent)
		{
			return i;
		}
	}

	return -1;
}


//this part that renders the world is really a camera-
//in a more detailed engine, we'd have a list of cameras, and then render manager would
//render the cameras in order
void SFRenderManager::RenderComponents()
{
	//Get the logical viewport so we can pass this to the SpriteComponents when it's draw time

	for (SFSpriteComponent* c : mComponents)
	{
			SFWindowManager::sInstance->draw(c->GetSprite());
	}
}

void SFRenderManager::Render()
{
	// Clear the back buffer
	SFWindowManager::sInstance->clear(sf::Color::Black);

	// The game has started.
	if (mComponents.size() > 0)
	{
		// Update the view position.
		UpdateView();

		SFRenderManager::sInstance->RenderTexturedWorld();

		SFRenderManager::sInstance->RenderComponents();

		

		// Draw UI elements.
		SFRenderManager::sInstance->RenderUI();

		//James - call method to check if the game is over
		GameIsOver();

		//James - if the game is over and we didn't finish all the laps, display lose screen
		if (GetIsGameOver() && weWon == false)
		{
			// Print you lose screen
			sf::Vector2f died(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
			m_loseScreen.setPosition(died);
			SFWindowManager::sInstance->draw(m_loseScreen);
		}
		else
		{
			//James - call method to check if the race has been finished and if they have won it
			//if they have finished all laps and crossed finishline, display win screen
			if(GetIsGameOver() && weWon == true)
			{
				// Draw the winner screen.
				sf::Vector2f winner(view.getCenter().x - view.getSize().x / 2, view.getCenter().y - view.getSize().y / 2);
				m_winnerScreen.setPosition(winner);
				SFWindowManager::sInstance->draw(m_winnerScreen);
			}

		}
	}
	// The game has not started.
	else
	{
		SFWindowManager::sInstance->draw(m_startScreen);
	}
	

	// Present our back buffer to our front buffer
	SFWindowManager::sInstance->display();
}