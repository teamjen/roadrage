#include <RoboCatClientPCH.h>
//changes by james and ken

std::unique_ptr<SFTextureManager> SFTextureManager::sInstance;

void SFTextureManager::StaticInit()
{
	sInstance.reset(new SFTextureManager());
}

//add in paths for textures
// James - Organise files paths
SFTextureManager::SFTextureManager()
{
	// Background textures
	CacheTexture("road", "../Assets/Background/road.png");
	CacheTexture("finish", "../Assets/Background/finish.png");
	CacheTexture("barrier", "../Assets/Background/barrier.png");
	CacheTexture("checkpoint", "../Assets/Background/checkpoint.png");
	CacheTexture("grass", "../Assets/Background/grass.png");

	// Player textures
	CacheTexture("carBlack", "../Assets/Player/carBlack.png");
	CacheTexture("carBlue", "../Assets/Player/carBlue.png");
	CacheTexture("carGreen", "../Assets/Player/carGreen.png");
	CacheTexture("carOrange", "../Assets/Player/carOrange.png");
	CacheTexture("carYellow", "../Assets/Player/carYellow.png");

	// Game screen textures
	CacheTexture("connectingToServer", "../Assets/Screens/connectingToServerScreen.png");
	CacheTexture("lose", "../Assets/Screens/loseScreen.png");
	CacheTexture("win", "../Assets/Screens/winScreen.png");

	// Gameplay textures
	CacheTexture("boost", "../Assets/Gameplay/boost.png");
	CacheTexture("missile", "../Assets/Gameplay/missile.png");
}

SFTexturePtr SFTextureManager::GetTexture(const string & inTextureName)
{
	return mNameToTextureMap[inTextureName];
}

bool SFTextureManager::CacheTexture(string inName, const char * inFileName)
{
	SFTexturePtr newTex(new sf::Texture());
	if (!newTex->loadFromFile(inFileName))
		return false;

	mNameToTextureMap[inName] = newTex;
	return true;
}
