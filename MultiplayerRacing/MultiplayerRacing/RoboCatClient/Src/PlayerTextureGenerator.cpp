//Chnages by James
#include <RoboCatClientPCH.h>

std::unique_ptr<PlayerTextureGenerator> PlayerTextureGenerator::sInstance;

//change ids of player textures
PlayerTextureGenerator::PlayerTextureGenerator()
{
	m_playerTextureIDs = {
		"carBlack", 
		"carBlue",
		"carGreen",
		"carOrange",
		"carYellow",

	};
}

SFTexturePtr PlayerTextureGenerator::GetPlayerTexure(uint32_t p_id)
{
	
	return SFTextureManager::sInstance->GetTexture(ResolveID(p_id));
}

std::string PlayerTextureGenerator::ResolveID(uint32_t p_id)
{
	return m_playerTextureIDs[p_id % m_playerTextureIDs.size()];
}

bool PlayerTextureGenerator::StaticInit()
{
	sInstance.reset(new PlayerTextureGenerator());
	return true;
}
