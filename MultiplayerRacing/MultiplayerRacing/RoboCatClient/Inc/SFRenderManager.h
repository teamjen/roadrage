#pragma once
//Changes made by James

class SFRenderManager : public RoboCat
{
public:

	static void StaticInit();
	static std::unique_ptr<SFRenderManager>	sInstance;

	void Render();
	void RenderComponents();

	//vert inefficient method of tracking scene graph...
	void AddComponent(SFSpriteComponent* inComponent);
	void RemoveComponent(SFSpriteComponent* inComponent);
	int	 GetComponentIndex(SFSpriteComponent* inComponent) const;

	sf::Vector2f FindCatCentre();

	//James - Method that triggers the win/ lose logic
	void GameIsOver();

	//James - Tell server we won so update the leaderboard
	
	


private:

	SFRenderManager();

	void RenderUI();
	void UpdateView();
	void RenderTexturedWorld();
	//James - Method for tracking how many laps the player has done
	void RaceProgress();
	//James - Method for counting laps
	void LapCounter();


	//James - booleans added to trigger win logic
	// Bytes used for lap counter
	// Bool weWon for if we finished the race first and won
	bool checkpoint1;
	bool finishline1;
	bool checkpoint2;
	bool finishline2;
	bool checkpoint3;
	bool raceFinished;
	bool weWon;
	BYTE currentLap = 1;
	BYTE totalLaps = 3;

	//this can't be only place that holds on to component- it has to live inside a GameObject in the world
	vector< SFSpriteComponent* > mComponents;
	sf::View view;
	sf::Sprite m_startScreen;
	sf::Sprite m_loseScreen;
	sf::Sprite m_winnerScreen;
	sf::Vector2f m_lastCatPos;
};
