//Changes by James
class RoboCat : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'RCAT', GameObject )

	enum ECatReplicationState
	{
		ECRS_Pose = 1 << 0,
		ECRS_Color = 1 << 1,
		ECRS_PlayerId = 1 << 2,
		ECRS_Speed = 1 << 3,
		ECRS_GameOver = 1 << 4,


		ECRS_AllState = ECRS_Pose | ECRS_Color | ECRS_PlayerId | ECRS_Speed | ECRS_GameOver
	};


	static	GameObject*	StaticCreate()			{ return new RoboCat(); }

	virtual uint32_t GetAllStateMask()	const override	{ return ECRS_AllState; }

	virtual	RoboCat*	GetAsCat()	{ return this; }

	virtual void Update()	override;

	void ProcessInput( float inDeltaTime, const InputState& inInputState );
	void SimulateMovement( float inDeltaTime );

	void ProcessCollisions();

	void		SetPlayerId( uint32_t inPlayerId )			{ mPlayerId = inPlayerId; }
	uint32_t	GetPlayerId()						const 	{ return mPlayerId; }

	void			SetVelocity( const Vector3& inVelocity )	{ mVelocity = inVelocity; }
	const Vector3&	GetVelocity()						const	{ return mVelocity; }

	//James - for returning the current maxSpeed
	uint8_t& GetMaxSpeed() { return mMaxSpeed; };

	bool GetIsGameOver();
	void SetIsGameOver(bool isIt);

	
	bool GetWeAreTheWinner();
	void SetWeAreTheWinner(bool winner);
	

	virtual uint32_t	Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const override;
	RoboCat();
protected:
	

private:

	
	void TryMove(Vector3 p_move);


	void	AdjustVelocityByThrust( float inDeltaTime );
	

	Vector3				mVelocity;
	

	float				mMaxRotationSpeed;
	float				mMaxLinearSpeed;

	//bounce fraction when hitting various things
	float				mWallRestitution;
	float				mCatRestitution;

	//James - for calculating mMaxLinearSpeed for player's velocity
	float maxSpeedMultiplier;

	uint32_t			mPlayerId;

protected:

	///move down here for padding reasons...
	
	float				mLastMoveTimestamp;

	sf::Vector2f		mThrustDir;
	//James - mMaxSpeed varibale for keeping track of updates to maxSpeed
	//gameOver keeps track if any player has finished all laps
	uint8_t					mMaxSpeed;
	bool					gameOver;
	bool					didWeWin;
	

	bool				mIsShooting;

};

typedef shared_ptr< RoboCat >	RoboCatPtr;