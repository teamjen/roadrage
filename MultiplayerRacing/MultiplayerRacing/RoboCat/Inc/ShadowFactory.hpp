
#ifndef SHADOWFACTORY_HPP
#define SHADOWFACTORY_HPP

class ShadowFactory
{
public:
	static void StaticInit();
	static std::unique_ptr<ShadowFactory>	sInstance;

	bool load();
	bool doesCollideWithWorld(sf::FloatRect p_bounds);

private:
	std::vector<sf::FloatRect> shadowCasters;
	sf::Vector2f m_worldSize;

	bool doesExitWithMapBounds(sf::FloatRect p_bounds);
	
};
#endif