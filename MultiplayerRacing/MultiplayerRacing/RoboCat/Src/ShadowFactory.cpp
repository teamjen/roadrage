#include "RoboCatPCH.h"

std::unique_ptr<ShadowFactory> ShadowFactory::sInstance;

void ShadowFactory::StaticInit()
{
	ShadowFactory* sf = new ShadowFactory();
	sf->load();
	sInstance.reset(sf);
}

bool ShadowFactory::load()
{
	// Might be able to perform some optimizations here.
	float rectSize = 64;

	sf::Image map;
	if (!map.loadFromFile("../Assets/maps/map.png"))
		return false;


	auto size = map.getSize();
	m_worldSize = sf::Vector2f(size.x * rectSize, size.y * rectSize);

	for (int i = 0; i < size.x; i++)
	{
		for (int j = 0; j < size.y; j++)
		{
			if (map.getPixel(i, j) == sf::Color::Black)
			{
				shadowCasters.push_back(sf::FloatRect(i * rectSize, j * rectSize, rectSize, rectSize));
			}
		}
	}

	return true;
}

bool ShadowFactory::doesCollideWithWorld(sf::FloatRect p_bounds)
{
	if (doesExitWithMapBounds(p_bounds))
		return true;

	for (auto s : shadowCasters)
	{
		if (s.intersects(p_bounds))
		{
			return true;
		}
	}
	return false;
}

bool ShadowFactory::doesExitWithMapBounds(sf::FloatRect p_bounds)
{
	// Check all sides of the map.
	if (p_bounds.top <= 0)
		return true;
	else if (p_bounds.left <= 0)
		return true;
	else if ((p_bounds.left + p_bounds.width) >= m_worldSize.x)
		return true;
	else if ((p_bounds.top + p_bounds.height) >= m_worldSize.y)
		return true;

	return false;
}


