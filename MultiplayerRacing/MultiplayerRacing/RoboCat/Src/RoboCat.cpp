//Changes by James
#include <RoboCatPCH.h>

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

RoboCat::RoboCat() :
	GameObject(),
	mMaxRotationSpeed( 5.f ),
	//James - for debugging while checking if the finishline works
	mMaxLinearSpeed( 0.f ),
	mVelocity( Vector3::Zero ),
	mWallRestitution( 0.1f ),
	mCatRestitution( 0.1f ),
	mThrustDir( sf::Vector2f(0.f, 0.f) ),
	mPlayerId( 0 ),
	mIsShooting( false ),
	//James - multiplier for calculating the maxLinearSpeed
	maxSpeedMultiplier(1000),
	//James - maxSpeed at start
	mMaxSpeed(6),
	//Debugging speed
	//mMaxSpeed(15),
	gameOver(false),
	didWeWin(false)
	
{
	SetCollisionRadius( 20.f );
}

void RoboCat::SetIsGameOver(bool isIt)
{
	this->gameOver = isIt;
}

bool RoboCat::GetIsGameOver()
{
	return this->gameOver;
}

void RoboCat::SetWeAreTheWinner(bool winner)
{
	this->didWeWin = winner;
}

bool RoboCat::GetWeAreTheWinner()
{
	return this->didWeWin;
}

void RoboCat::ProcessInput( float inDeltaTime, const InputState& inInputState )
{
	//process our input....

	int rot = -1;
	int W = 0;
	int NW = 45;
	int N = 90;
	int NE = 135;
	int E = 180;
	int SE = 225;
	int S = 270;
	int SW = 315;


	float inputHorizontalDelta = inInputState.GetDesiredHorizontalDelta();
	mThrustDir.x = inputHorizontalDelta;
	float inputForwardDelta = inInputState.GetDesiredVerticalDelta();
	mThrustDir.y = -inputForwardDelta;

	
	if (mThrustDir.x == 1 && mThrustDir.y == 1)
		rot = NE;
	else if (mThrustDir.x == 1 && mThrustDir.y == -1)
		rot = NW;
	else if (mThrustDir.x == -1 && mThrustDir.y == 1)
		rot = SE;
	else if (mThrustDir.x == -1 && mThrustDir.y == -1)
		rot = SW;
	else if (mThrustDir.x == 1 && mThrustDir.y == 0)
		rot = N;
	else if (mThrustDir.x == -1 && mThrustDir.y == 0)
		rot = S;
	else if (mThrustDir.x == 0 && mThrustDir.y == 1)
		rot = E;
	else if (mThrustDir.x == 0 && mThrustDir.y == -1)
		rot = W;
	
	if (rot != -1)
		SetRotation(rot);

	mIsShooting = inInputState.IsShooting();
}

//James - if the player travels diagonally, make sure they travel the same speed as in x,y directions 
void RoboCat::AdjustVelocityByThrust( float inDeltaTime )
{
	//James - set the maxlinear speed to the players current speed
	//If the player gets pickup or damaged, their speed will be updated
	//The getmaxspeed variable is purposely low so that it can be sent in packets using only 4 bits 
	//and a caluclation is done to bring it to its real value
	mMaxLinearSpeed = GetMaxSpeed() * maxSpeedMultiplier;


	//James - track x and y velocity values
	//if they become more than the maxlinearspeed, set them to maxlinearspeed
	mVelocity.mX = (GetForwardVector().mX + (mThrustDir.x * inDeltaTime * mMaxLinearSpeed));
	mVelocity.mY = (GetForwardVector().mY + (mThrustDir.y * inDeltaTime * mMaxLinearSpeed));
	mVelocity.mZ = GetForwardVector().mZ;

	//a*a + B*b = c*c
	float actualVelocity = sqrt((mThrustDir.x * mThrustDir.x) + (mThrustDir.y * mThrustDir.y));
	float maxVelocity = (GetForwardVector().mX + (1 * inDeltaTime * mMaxLinearSpeed));

	if ((mVelocity.mX > maxVelocity && mVelocity.mY > maxVelocity) || 
		(mVelocity.mX < -maxVelocity && mVelocity.mY < -maxVelocity))
	{
		mVelocity *= mMaxLinearSpeed / actualVelocity;

		mVelocity.mX = (GetForwardVector().mX + (actualVelocity * inDeltaTime * mMaxLinearSpeed));
		mVelocity.mY = (GetForwardVector().mY + (actualVelocity * inDeltaTime * mMaxLinearSpeed));
	}

	mVelocity = Vector3(mVelocity.mX, mVelocity.mY, mVelocity.mZ);
}


void RoboCat::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust( inDeltaTime );
	
	// Replace with a "TryMove" that preemptively checks for collisions.
	TryMove(mVelocity * inDeltaTime);
	//SetLocation( GetLocation() + mVelocity * inDeltaTime );

	// Will encompass the collisions with everything except the walls.
	ProcessCollisions();
}

void RoboCat::Update()
{
	
}

void RoboCat::TryMove(Vector3 p_move)
{
	// SetLocation() to update location.
	sf::Vector2f move(p_move.mX, p_move.mY);
	
	// Make a collision box in here for the player, square around the player.
	float size = 25;
	// Position at the centre of the player.
	Vector3 playerPosition = GetLocation();
	sf::FloatRect player(playerPosition.mX - (size / 2), playerPosition.mY - (size / 2), size, size);

	player.left += move.x;
	player.top += move.y;

	if (!ShadowFactory::sInstance->doesCollideWithWorld(player))
	{
		SetLocation(playerPosition + p_move);
		return;
	}

	// Move checks for single dimension movement.
	player.top -= move.y;
	if (!ShadowFactory::sInstance->doesCollideWithWorld(player))
	{
		SetLocation(playerPosition + Vector3(p_move.mX, 0, 0));
		return;
	}
	
	player.top += move.y;
	player.left -= move.x;
	if (!ShadowFactory::sInstance->doesCollideWithWorld(player))
	{
		SetLocation(playerPosition + Vector3(0, p_move.mY, 0));
		return;
	}
}

void RoboCat::ProcessCollisions()
{
	//right now just bounce off the sides..
	//ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	for( auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt )
	{
		GameObject* target = goIt->get();
		if (target->GetClassId() == 'MOUS')
		{
		}
		if( target != this && !target->DoesWantToDie() )
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			Vector3 targetLocation = target->GetLocation();
			float targetRadius = target->GetCollisionRadius();

			Vector3 delta = targetLocation - sourceLocation;
			float distSq = delta.LengthSq2D();
			float collisionDist = ( sourceRadius + targetRadius );
			if( distSq < ( collisionDist * collisionDist ) )
			{
				//first, tell the other guy there was a collision with a cat, so it can do something...

				if( target->HandleCollisionWithCat( this ) )
				{
					//okay, you hit something!
					//so, project your location far enough that you're not colliding
					Vector3 dirToTarget = delta;
					dirToTarget.Normalize2D();
					Vector3 acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist;
					//important note- we only move this cat. the other cat can take care of moving itself
					SetLocation( targetLocation - acceptableDeltaFromSourceToTarget );

					
					Vector3 relVel = mVelocity;
				
					//if other object is a cat, it might have velocity, so there might be relative velocity...
					RoboCat* targetCat = target->GetAsCat();
					if( targetCat )
					{
						relVel -= targetCat->mVelocity;
					}

					//got vel with dir between objects to figure out if they're moving towards each other
					//and if so, the magnitude of the impulse ( since they're both just balls )
					float relVelDotDir = Dot2D( relVel, dirToTarget );

					if (relVelDotDir > 0.f)
					{
						Vector3 impulse = relVelDotDir * dirToTarget;
					
						if( targetCat )
						{
							mVelocity -= impulse;
							mVelocity *= mCatRestitution;
						}
						else
						{
							mVelocity -= impulse * 2.f;
							mVelocity *= mWallRestitution;
						}
					}
				}
			}
		}
	}

}

uint32_t RoboCat::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & ECRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetPlayerId() );

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}


	if( inDirtyState & ECRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 velocity = mVelocity;
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//always write mThrustDir- it's just two bits
	if( mThrustDir != sf::Vector2f(0.f,0.f))
	{
		inOutputStream.Write( true );
		inOutputStream.Write(mThrustDir.x > 0.f);
		inOutputStream.Write(mThrustDir.y > 0.f);
	}
	else
	{
		inOutputStream.Write( false );
	}

	if( inDirtyState & ECRS_Color )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetColor() );

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}
	//James - write maxSpeed for player using 4 bits
	if( inDirtyState & ECRS_Speed )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write(mMaxSpeed, 4);

		writtenState |= ECRS_Speed;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}
	//James - write gameOver using 1 bit
	if (inDirtyState & ECRS_GameOver)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(this->gameOver);

		writtenState |= ECRS_GameOver;
	}
	else
	{
		inOutputStream.Write(false);
	}

	return writtenState;
}
